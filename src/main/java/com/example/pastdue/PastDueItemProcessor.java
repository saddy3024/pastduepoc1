package com.example.pastdue;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.pastdue.SpringBatchDemoApplication.PastDue;

public class PastDueItemProcessor implements ItemProcessor<PastDue, PastDue>
{

    @Override
    public PastDue process (PastDue pastDue) throws Exception
    {
        
        if(pastDue.getAmount() > 500 && pastDue.getPastDueDays() > 180) {
        	pastDue.setPastDue_AmtGt500_DaysGt180("TRUE");
        } else {
        	pastDue.setPastDue_AmtGt500_DaysGt180("FALSE");
        }
        return pastDue;
    }


}
