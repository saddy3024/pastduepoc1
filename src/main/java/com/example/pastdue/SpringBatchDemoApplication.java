package com.example.pastdue;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.ResourceUtils;

@SpringBootApplication
@EnableBatchProcessing
public class SpringBatchDemoApplication {

	public static void main(String[] args) {
		System.exit(SpringApplication.exit(
			SpringApplication.run(SpringBatchDemoApplication.class, args)));
	}
	
	@Bean
	@StepScope
	public Resource resource(@Value("#{jobParameters['fileName']}") String fileName) throws MalformedURLException, FileNotFoundException {
		return new ClassPathResource(fileName);
	}

	@Bean
	public FlatFileItemReader<PastDue> itemReader(Resource resource)  {
		return new FlatFileItemReaderBuilder<PastDue>()
				.name("PastDueItemReader")
				.resource(resource)
				.delimited()
				.names("firstName", "lastName","ccNum","amount","pastDueDays","isPastDue_AmtGt500_DaysGt180")
				.targetType(PastDue.class)
				.build();
	}

	@Bean
	public PastDueItemProcessor itemProcessor() {
		// TODO Auto-generated method stub
		return new PastDueItemProcessor();
	}

	@Bean
	public JdbcBatchItemWriter<PastDue> itemWriter(DataSource dataSource) {
		return new JdbcBatchItemWriterBuilder<PastDue>()
				.dataSource(dataSource)
				.sql("INSERT INTO CC_PAST_DUE (FIRST_NAME, LAST_NAME, CC_NUMBER, AMOUNT, PAST_DUE_DAYS, PAST_DUE_AMT_500_DAY_180) VALUES (:firstName, :lastName,:ccNum,:amount,:pastDueDays,:isPastDue_AmtGt500_DaysGt180)")
				.beanMapped()
				.build();
	}

	@Bean
	public Job job(JobBuilderFactory jobs, StepBuilderFactory steps,
				   DataSource dataSource, Resource resource) {
		return jobs.get("job")
				.start(steps.get("step")
						.<PastDue, PastDue>chunk(3)
						.reader(itemReader(resource))
						.<PastDue,PastDue>processor(itemProcessor())
						.writer(itemWriter(dataSource))
						.build())
				.build();
	}


	public static class PastDue {
		public PastDue() {}
		public PastDue(String firstName, String lastName, String ccNum, float amount, int pastDueDays,
				String isPastDue_AmtGt500_DaysGt180) {
			super();
			this.firstName = firstName;
			this.lastName = lastName;
			this.ccNum = ccNum;
			this.amount = amount;
			this.pastDueDays = pastDueDays;
			this.isPastDue_AmtGt500_DaysGt180 = isPastDue_AmtGt500_DaysGt180;
		}
		private String firstName;
		private String lastName;
		private String ccNum;
		private float amount;
		private int pastDueDays;
		private String isPastDue_AmtGt500_DaysGt180;
		
		public String getCcNum() {
			return ccNum;
		}
		public void setCcNum(String ccNum) {
			this.ccNum = ccNum;
		}
		public float getAmount() {
			return amount;
		}
		public void setAmount(float amount) {
			this.amount = amount;
		}
		public int getPastDueDays() {
			return pastDueDays;
		}
		public void setPastDueDays(int pastDueDays) {
			this.pastDueDays = pastDueDays;
		}
		public String isPastDue_AmtGt500_DaysGt180() {
			return isPastDue_AmtGt500_DaysGt180;
		}
		public void setPastDue_AmtGt500_DaysGt180(String isPastDue_AmtGt500_DaysGt180) {
			this.isPastDue_AmtGt500_DaysGt180 = isPastDue_AmtGt500_DaysGt180;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
                
	}

}